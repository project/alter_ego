<?php

/**
 * @file
 * Implements all functions and classes for the Avatar entity.
 *
 */


/**
* Determines whether the given user has access to a avatar.
*
* @param $op
*   The operation being performed. One of 'view', 'update', 'create', 'delete'
*   or just 'edit' (being the same as 'create' or 'update').
* @param $avatar
*   Optionally a avatar or a avatar type to check access for. If nothing is
*   given, access for all avatars is determined.
* @param $account
*   The user to check for. Leave it to NULL to check for the global user.
* @return boolean
*   Whether access is allowed or not.
*/
function avatar_access($op, $avatar = NULL, $account = NULL) {
  if (user_access('administer avatars', $account)) {
    return TRUE;
  }
  if (isset($avatar) && $type_name = $avatar->type) {
    switch ($op) {
      case 'view':
        if (user_access("$op any $type_name avatar", $account)) {
          return TRUE;
        }
        break;
      case 'edit':
        if ($account->uid == $avatar->uid && user_access("$op own $type_name avatar", $account)) {
          return TRUE;
        }
        elseif (user_access("$op any $type_name avatar", $account)) {
          return TRUE;
        }
        break;
      case 'create':
        if (user_access("create $type_name avatar", $account)) {
          return TRUE;
        }
    }
  }
  return FALSE;
}


/**
* Fetch a avatar object. Make sure that the wildcard you choose
* in the avatar entity definition fits the function name here.
*
* @param $tid
*   Integer specifying the avatar id.
* @param $reset
*   A boolean indicating that the internal cache should be reset.
* @return
*   A fully-loaded $avatar object or FALSE if it cannot be loaded.
*
* @see mmoguild_load_multiple()
*/
function avatar_load($aid, $reset = FALSE) {
  $avatars = avatar_load_multiple(array($aid), array(), $reset);
  return reset($avatars);
}


/**
 * Load multiple avatars based on certain conditions.
 *
 * @param $tids
 *   An array of avatar IDs.
 * @param $conditions
 *   An array of conditions to match against the {avatar} table. [DEPERCIATED]
 * @param $reset
 *   A boolean indicating that the internal cache should be reset.
 * @return
 *   An array of avatar objects, indexed by tid.
 *
 * @see entity_load()
 * @see mmoguild_load()
 */
function avatar_load_multiple($aids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('avatar', $aids, $conditions, $reset);
}


/**
 * Deletes a avatar.
 */
function avatar_delete(Avatar $avatar) {
  $avatar->delete();
}


/**
 * Delete multiple avatars.
 *
 * @param $tids
 *   An array of avatar IDs.
 */
function avatar_delete_multiple(array $aids) {
  entity_get_controller('avatar')->delete($aids);
}


/**
 * Create a avatar object.
 */
function avatar_create($values = array()) {
  return entity_get_controller('avatar')->create($values);
}


/**
 * Saves a avatar to the database.
 *
 * @param $avatar
 *   The avatar object.
 */
function avatar_save(Avatar $avatar) {
  return $avatar->save();
}


/**
 * URI callback for avatars
 */
function avatar_uri(Avatar $avatar) {
  return array(
    'path' => 'avatar/' . $avatar->aid,
  );
}


/**
 * Menu title callback for showing individual entities
 */
function avatar_page_title(Avatar $avatar) {
  return $avatar->name;
}

/**
 * Sets up content to show an individual avatar
 */
function avatar_view($avatar, $view_mode = 'full') {
  $controller = entity_get_controller('avatar');
  $content = $controller->view(array($avatar->aid => $avatar), $view_mode);
  return $content;
}

/**
 * Sets up content to show an individual avatar
 */
function avatar_page_view($avatar, $view_mode = 'full') {
  $content = avatar_view($avatar, $view_mode);
  if ($view_mode == 'popup') {
    // If we are poping up just send this text, not the whole page.
    print(render($content));
  }else {
    return $content;
  }
  
}


/**
 *
 * AvatarType Functions / Classes
 *
 */

/**
* Access callback for the entity API.
*/
function avatar_type_access($op, $type = NULL, $account = NULL) {
  return user_access('administer avatar types', $account);
}

/**
 * Gets an array of all avatar types, keyed by the type name.
 *
 * @param $type_name
 *   If set, the type with the given name is returned.
 * @return AvatarType[]
 *   Depending whether $type isset, an array of avatar types or a single one.
 */
function avatar_get_types($type_name = NULL) {
  // entity_load will get the Entity controller for our avatar entity and call the load
  // function of that object - we are loading entities by name here.
  $types = entity_load_multiple_by_name('avatar_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}


/**
 * Menu argument loader; Load a avatar type by string.
 *
 * @param $type
 *   The machine-readable name of a avatar type to load.
 * @return
 *   A avatar type array or FALSE if $type does not exist.
 */
function avatar_type_load($type) {
  return avatar_get_types($type);
}





/**
 * Saves a avatar type to the db.
 */
function avatar_type_save(AvatarType $type) {
  $type->save();
}


/**
 * Deletes a avatar type from the db.
 */
function avatar_type_delete(AvatarType $type) {
  $type->delete();
}


/**
* The class used for avatar entities
*/
class Avatar extends Entity {

  public function __construct($values = array()) {
    parent::__construct($values, 'avatar');
  }

  /**
   *
   * @see Entity::save()
   * @see entity_save()
   */
  public function save() {
    if (!empty($this->uid)) {
      // If this user hasn't set a main toon.  Use this one.
      if (alter_ego_get_main_aid($this->uid) === FALSE) {
        $this->is_main = TRUE;
        drupal_static_reset('mmoguild_get_main_aid');
      }
    }
    return parent::save();
  }


  protected function defaultLabel() {
    return $this->name;
  }

  protected function defaultUri() {
    return array('path' => 'avatar/' . $this->aid);
  }


}

/**
* The Controller for Avatar entities
*/
class AvatarController extends EntityAPIController {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }


  /**
   * Create a avatar - we first set up the values that are specific
   * to our avatar schema but then also go through the EntityAPIController
   * function.
   *
   * @param $type
   *   The machine-readable type of the avatar.
   *
   * @return
   *   A avatar object with all default fields initialized.
   */
  public function create(array $values = array()) {
    // Add values that are specific to our Avatar
    $values += array(
      'aid' => '',
      'is_new' => TRUE,
      'title' => '',
      'created' => '',
      'changed' => '',
      'data' => '',
    );

    //$avatar = parent::create($values);
    // Add is_new property if it is not set.
    $values += array('is_new' => TRUE);

    // See if we've defined a custom class for this entity type.
    if (is_array($this->entityInfo['custom entity class'])) {
      if (array_key_exists($values['type'], $this->entityInfo['custom entity class'])) {
        if ($class_name = $this->entityInfo['custom entity class'][$values['type']]) {
          return new $class_name($values, $this->entityType);
        }
      }
    }
    // See if we are using entity_class
    if (isset($this->entityInfo['entity class'])) {
      if ($class_name = $this->entityInfo['entity class']) {
        return new $class_name($values, $this->entityType);
      }
    }
    return (object) $values;
  }

  /**
   * Builds and executes the query for loading.
   *
   * @return The results in a Traversable object.
   */
  public function query($ids, $conditions, $revision_id = FALSE) {

    // Only cast into custom classes if base class is defined.
    if (!empty($this->entityInfo['entity class'])) {
      $results = array();

      // Load created Avatar Types
      $avatar_types = avatar_get_types();
      $load_into = array();
      // If we have defined custom entity classes
      if (is_array($this->entityInfo['custom entity class'])) {
        foreach ($avatar_types as $avatar_type => $value) {
          // If this type has a custom class, place it into the load_into variable.
          if (array_key_exists($avatar_type, $this->entityInfo['custom entity class'])) {
            $load_into[$avatar_type] = $this->entityInfo['custom entity class'][$avatar_type];
          }
        }
      }

      // If we have any custom classes, load them first.
      foreach ($load_into as $avatar_type => $class_name) {
        $query = $this->buildQuery($ids, $conditions, $revision_id);
        $query->condition('type', $avatar_type);
        $result = $query->execute();
        $result->setFetchMode(PDO::FETCH_CLASS, $class_name, array(array(), $this->entityType));
        $results += $result->fetchAllAssoc($this->idKey);
      }

      // Load other types.
      $query = $this->buildQuery($ids, $conditions, $revision_id);
      if (!empty($load_into)) {
        $query->condition('type', array_keys($load_into), 'NOT IN');
      }
      $result = $query->execute();
      $result->setFetchMode(PDO::FETCH_CLASS, $this->entityInfo['entity class'], array(array(), $this->entityType));
      $results += $result->fetchAllAssoc($this->idKey);

      return $results;

    }
    else {
      // Build the query.
      $query = $this->buildQuery($ids, $conditions, $revision_id);
      $result = $query->execute();
    }
    return $result;
  }
}


/**
* The class used for avatar type entities
*/
class AvatarType extends Entity {

  public $type;
  public $label;

  public function __construct($values = array()) {
    parent::__construct($values, 'avatar_type');
  }

}


/**
* The Controller for Avatar entities
*/
class AvatarTypeController extends EntityAPIControllerExportable {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  /**
   * Create a avatar type - we first set up the values that are specific
   * to our avatar type schema but then also go through the EntityAPIController
   * function.
   *
   * @param $type
   *   The machine-readable type of the avatar.
   *
   * @return
   *   A avatar type object with all default fields initialized.
   */
  public function create(array $values = array()) {
    // Add values that are specific to our Avatar
    $values += array(
      'id' => '',
      'is_new' => TRUE,
      'data' => '',
    );
    $avatar_type = parent::create($values);
    return $avatar_type;
  }
}