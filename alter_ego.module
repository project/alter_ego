<?php

/**
 * @file
 * Module for the Avatar Entity - a starting point to create your own Entity
 * and associated administration interface
 */

/**
 * Implement hook_init().
 *
 * Add required javascript libraries.
 */
function alter_ego_init() {
  drupal_add_library('system', 'ui.autocomplete');
  drupal_add_library('system', 'ui.position');
}

// All Entity classes / function are in a seperate file.  Class autoloading does not seem to work during install/uninstalls and throws errors if used.
require_once(DRUPAL_ROOT . '/' . drupal_get_path('module', 'alter_ego') . '/avatar.entity.inc');

/**
 * Implement hook_entity_info().
 *
 * We define two entities here - the actual entity that will hold our domain
 * specific information and an entity that holds information about the different
 * types of entities. See here: http://drupal.org/node/977380 for a discussion on this
 * choice.
 */
function alter_ego_entity_info() {
  $return = array();
  $return['avatar'] = array(
    'label' => t('Avatar'),
    // The entity class and controller class extend the classes provided by the
    // Entity API
    'entity class' => 'Avatar',
    'controller class' => 'AvatarController',
    'file' => 'avatar.entity.inc',
    'base table' => 'avatar',
    'fieldable' => TRUE,
    'entity keys' => array(
        'id' => 'aid',
        'bundle' => 'type',
    ),
    // Bundles are defined by the avatar types below
    'bundles' => array(),
    // Bundle keys tell the FieldAPI how to extract information from the bundle objects
    'bundle keys' => array(
      'bundle' => 'type',
    ),
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
    'creation callback' => 'avatar_create',
    'access callback' => 'avatar_access',
    'module' => 'alter_ego',
    // The information below is used by the AvatarUIController (which extends the EntityDefaultUIController)
    'admin ui' => array(
      'path' => 'admin/content/avatars',
      'file' => 'avatar.admin.inc',
      'controller class' => 'AvatarUIController',
      'menu wildcard' => '%avatar',
    ),
    'view modes' => array(
      'full' => array(
        'label' => t('Full content'),
        'custom settings' => FALSE,
      ),
      'teaser' => array(
        'label' => t('Teaser'),
        'custom settings' => TRUE,
      ),
      'popup' => array(
        'label' => t('Popup'),
        'custom settings' => TRUE,
      ),
      'select_block' => array(
        'label' => t('Block Render'),
        'custom settings' => TRUE,
      ),
      'select_block_pulldown' => array(
        'label' => t('Block Pulldown Render'),
        'custom settings' => TRUE,
      ),
      'rss' => array(
        'label' => t('RSS'),
        'custom settings' => FALSE,
      ),
    ),
  );
  // The entity that holds information about the entity types
  $return['avatar_type'] = array(
    'label' => t('Avatar Type'),
    'entity class' => 'AvatarType',
    'controller class' => 'AvatarTypeController',
    'base table' => 'avatar_type',
    'fieldable' => FALSE,
    'bundle of' => 'avatar',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'name' => 'type',
      'label' => 'label',
    ),
    'access callback' => 'avatar_type_access',
    'module' => 'alter_ego',
    // Enable the entity API's admin UI.
    'admin ui' => array(
      'path' => 'admin/structure/avatar_types',
      'file' => 'avatar_type.admin.inc',
      'controller class' => 'AvatarTypeUIController',
    ),
  );
  return $return;
}


/**
 * Implement hook_entity_info_alter().
 *
 * We are adding the info about the avatar types via a hook to avoid a recursion
 * issue as loading the avatar types requires the entity info as well.
 *
 * @see model_entity_info_alter()
 */
function alter_ego_entity_info_alter(&$entity_info) {
  foreach (avatar_get_types() as $type => $info) {
    $entity_info['avatar']['bundles'][$type] = array(
      'label' => $info->label,
      'admin' => array(
        'path' => 'admin/structure/avatar_types/manage/%avatar_type',
        'real path' => 'admin/structure/avatar_types/manage/' . $type,
        'bundle argument' => 4,
        'access arguments' => array('administer avatar types'),
      ),
    );
  }
}

/**
 * Implement hook_permission().
 */
function alter_ego_permission() {
  // We set up permisssions to manage entity types, manage all entities and the
  // permissions for each individual entity
  $permissions = array(
    'administer avatar types' => array(
      'title' => t('Administer avatar types'),
      'description' => t('Create and delete fields for avatar types, and set their permissions.'),
    ),
    'administer avatars' => array(
      'title' => t('Administer avatars'),
      'description' => t('Edit and delete all avatars'),
    ),
  );
  
  //Generate permissions per avatar
  foreach (avatar_get_types() as $type) {
    $type_name = check_plain($type->type);
    $permissions += array(
      "create $type_name avatar" => array(
        'title' => t('%type_name: Create avatar', array('%type_name' => $type->label)),
      ),
      "edit own $type_name avatar" => array(
        'title' => t('%type_name: Edit own avatar', array('%type_name' => $type->label)),
      ),
      "edit any $type_name avatar" => array(
        'title' => t('%type_name: Edit any avatar', array('%type_name' => $type->label)),
      ),
      "view any $type_name avatar" => array(
        'title' => t('%type_name: View any avatar', array('%type_name' => $type->label)),
      ),
    );
  }
  return $permissions;
}


/**
 * Implement hook_menu().  Unused.
function alter_ego_menu() {
  $items = array();
  return $items;
}
 */

/**
 * Implement hook_views_api().
 * Required to provide views in modules.
 */
function alter_ego_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'alter_ego') . '/views',
  );
}


/**
 * Implement hook_theme().
 */
function alter_ego_theme() {
  return array(
    'avatar' => array(
      'render element' => 'elements',
      'template' => 'avatar',
    ),
    'avatar_add_list' => array(
      'variables' => array('content' => array()),
      'file' => 'avatar.admin.inc',
    )
  );
}

/**
 * Implement hook_preprocess_entity().
 *
 * Using this because template_preprocess_avatar does not get called properly.
 *
 * @param array $variables
 */
function alter_ego_preprocess_entity(&$variables) {
  if ($variables['entity_type'] == 'avatar') {
    if ($variables['view_mode'] == 'select_block_pulldown') {
      $avatar = $variables['avatar'];
      
      $variables['title_link_class'] = 'select-avatar-block-select-item';
      $variables['title_link_rel'] = $avatar->aid;
    }
  }
}
/**
 * This doesn't get called properly!!!  theme_suggestions don't call the correct preprocess.
 * @see hook_preprocess_entity() instead.
 *
 * @param array $variables
function template_preprocess_avatar(&$variables) {
}
 */

/**
 * Implement hook_block_info().
 */
function alter_ego_block_info() {
  $blocks['alter_ego_select_avatar'] = array(
    'info' => t('Avatar Select'),
    'cache' => DRUPAL_NO_CACHE,
    'region' => 'sidebar_first',
    'weight' => -1000,
    'status' => 1,
  );
  return $blocks;
}


/**
 * Implement hook_block_view().
 */
function alter_ego_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'alter_ego_select_avatar':
      global $user;
      // User is logged in.
      if (!empty($user->name)) {
        // Get add avatar link(s)
        $links = array();
        foreach (avatar_get_types() as $type) {
          $temp = avatar_create(array('type' => $type->type));
          if (avatar_access('create', $temp)) {
            $links[] = array(
                'title' => 'Add ' . $type->label,
                'href' => 'admin/content/avatars/add/' . $type->type,
            );
          }
        }
        
        $avatars = alter_ego_get_user_avatars();
        $aid = alter_ego_get_current_aid();
        if (!empty($avatars)) {
          // User has logged in and has an associated toon.
          $block['subject'] = '';//$avatars[$aid]->name;
          $block['content'] = array();
          
          $block['content']['current_avatar'] = avatar_view($avatars[$aid], 'select_block');
          
          $block['content']['avatar_pulldowns'] = array();
          $block['content']['avatar_pulldowns']['renders'] = array();
          foreach ($avatars as $avatar) {
            if ($avatar->aid != $aid) {
              $render = avatar_view($avatar, 'select_block_pulldown');
              $render['#prefix'] = '<li class="select-avatar-block-select-li">';
              $render['#suffix'] = '</li>';
              $block['content']['avatar_pulldowns']['renders'][] = $render;
            }
          }
          // If we have pulldowns, add the javascript prefix to pop it up.
          if (!empty($block['content']['avatar_pulldowns']['renders'])) {
            $block['content']['avatar_pulldowns']['#prefix'] = '<div id="select-avatar-block-char-arrow"></div><ul id="select-avatar-block-pulldown">';
            $block['content']['avatar_pulldowns']['#suffix'] = '</ul>';
          }
          $form = drupal_get_form('alter_ego_set_active_avatar_form');
          $block['content']['form'] = $form;
          
          if (!empty($links)) {
            $block['content']['add_links'] = array(
              '#theme' => 'links',
              '#links' => $links,
              '#attributes' => array(
                'class' => array('links', 'inline', 'operations')
               ),
               '#prefix' => '<div id="select-avatar-block-add">',
               '#suffix' => '</div>'
            );
          }
        }
        else {
          // User is logged in, but has not selected a toon.
          if (!empty($links)) {
            $block['subject'] = t('My Avatars');
            $block['content'] = t('You have not created your character yet!');
            $block['content'] .= '<div id="select-avatar-block-add">' . theme('links', array('links' => $links)) . '</div>';
          }
          else {
            // No avatar types have been defiend, return nothing.
          }
        }
      }
      else {
        // Not logged in, return nothing.
      }
      break;
  }
  return $block;
}
      
/**
 * Implement hook_menu_local_tasks_alter().
 */
function alter_ego_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  if ($root_path == 'admin/content/avatars') {
    $item = menu_get_item('admin/content/avatars/add');
    if ($item['access']) {
      $data['actions']['output'][] = array(
        '#theme' => 'menu_local_action',
        '#link' => $item,
      );
    }
  }
}

/**
 * Process variables for alter-ego-select-avatar-block.tpl.php
 * Is the main template for the block.
 *
 * @see wowtoon_block_view()
 */
function template_preprocess_alter_ego_select_avatar_block(&$variables) {
  global $user;
  if (array_key_exists($variables['selected_aid'], $variables['avatars'])) {
    $variables['current_avatar'] = $variables['avatars'][$variables['selected_aid']];
  }
  else {
    // This shouldn't happen, but verify that selected toon is a valid toon.
    $variables['current_avatar'] = reset($variables['avatars']);
    $variables['selected_aid'] = $variables['current_avatar']->aid;
    $selected_aids = variable_get('alter_ego_selected_aids', array());
    $selected_aids[$user->uid] = $variables['selected_aid'];
    variable_set('alter_ego_selected_aids', $selected_aids);
  }
  
  foreach ($variables['avatars'] as $avatar) {
    $variables['avatars'][$avatar->aid]->namelink = l($avatar->name, 'avatar/' . $avatar->aid, array('attributes' => array('class' => array('np'))));
  }
  
  $avatar = $variables['avatars'][$variables['selected_aid']];
  
  $variables['name'] = check_plain($avatar->name);
  $variables['name_link'] = l($avatar->name, 'avatar/' . $avatar->aid, array('attributes' => array('class' => array('np'))));
  
}



/**
 * Define the form to be placed in the select_avatar_block to allow the user to swap characters.  (Used if user does not have javascript enabled).
 *
 * @param array $form
 * @param array $form_state
 */
function alter_ego_set_active_avatar_form($form, &$form_state) {
  $avatars = alter_ego_get_user_avatars();
  $aid = alter_ego_get_current_aid();
  if ($aid) {
    $options = array($aid => $avatars[$aid]->name);
    unset($avatars[$aid]);
    foreach ($avatars as $avatar) {
      $options[$avatar->aid] = $avatar->name;
    }
  }
  $form['select_avatar'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $aid
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go')
  );
  return $form;
}

/**
 * Save selected avatar and view it.
 *
 * @param array $form
 * @param array $form_state
 */
function alter_ego_set_active_avatar_form_submit($form, &$form_state) {
  global $user;
  $values = $form_state['values'];
  alter_ego_set_current_aid($values['select_avatar']);
  $avatar = avatar_load($values['select_avatar']);
  $path = avatar_uri($avatar);
  $form_state['redirect'] = $path['path'];
}



/**
 * Implement hook_user_view().
 *
 * Display the user's main character on their page.
 *
 * @param $account
 *   The user account being viewed.
 * @param string $view_mode
 * @param string $langcode
 */
function alter_ego_user_view($account, $view_mode, $langcode) {
  $account->content['main_avatar'] = array(
    '#type' => 'fieldset',
    '#title' => 'My Avatar',
    '#weight' => -10,
  );
  
  $main_aid = alter_ego_get_main_aid($account->uid);
  if (!empty($main_aid)) {
    $avatar = avatar_load($main_aid);
    $account->content['main_avatar']['avatar'] = avatar_view($avatar, 'teaser');
  }
  $links = array();
  foreach (avatar_get_types() as $type) {
    $temp = avatar_create(array('type' => $type->type));
    if (avatar_access('create', $temp)) {
      $links[] = array(
        'title' => 'Add ' . $type->label . ' Avatar',
        'href' => 'admin/content/avatars/add/' . $type->type,
        'query' => array('destination' => 'user/' . $account->uid)
      );
    }
  }
  if (!empty($links)) {
    $account->content['main_avatar']['add_links'] = array(
      '#theme' => 'links',
      '#links' => $links,
      '#attributes' => array(
        'class' => array('links', 'inline', 'operations')
      )
    );
  }
}

/**
 * Implement hook_form_user_profile_form_alter().
 *
 * Hook into the user profile form to show and select the user's main character.
 *
 * @param array $form
 * @param array $form_state
 */
function alter_ego_form_user_profile_form_alter(&$form, &$form_state) {

  // Move Character Items into new fieldset.
  $form['main_avatar'] = array(
    '#type' => 'fieldset',
    '#title' => 'My Avatar',
    '#weight' => -10,
  );
  //$main_tid = $form['#user']->data['avatar']['maintid'];
  $main_aid = alter_ego_get_main_aid($form['#user']->uid);
  if (!empty($main_aid)) {
    $avatar = avatar_load($main_aid);
    $account->content['main_avatar']['avatar'] = avatar_view($avatar, 'teaser');
  }
  
  $avatars = alter_ego_get_user_avatars($form['#user']->uid);
  if (empty($avatars)) {
    $form['main_avatar']['avatar'] = array(
      '#prefix' => '<div>',
      '#markup' => t('You have not added your character to your account.'),
      '#suffix' => '</div>'
    );
  }
  else {
    if (!empty($main_aid)) {
      $toon = avatar_load($main_aid);
      $form['main_avatar']['avatar'] = avatar_view($avatar, 'teaser');
    }
    else {
      $form['main_avatar']['avatar'] = array(
            '#prefix' => '<div>',
            '#markup' => t('You have not selected your main character.'),
            '#suffix' => '</div>'
      );
    }
    $options = array();
    foreach ($avatars as $avatar) {
      $options[$avatar->aid] = $avatar->name;
    }
    if (count($options) >= 1) {
      $form['main_avatar']['mainaid'] = array(
        '#title' => t('Select your primary avatar'),
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => $main_aid,
      );
    }
  }
  $links = array();
  foreach (avatar_get_types() as $type) {
    $temp = avatar_create(array('type' => $type->type));
    if (avatar_access('create', $temp)) {
      $links[] = array(
          'title' => 'Add ' . $type->label,
          'href' => 'admin/content/avatars/add/' . $type->type,
          'query' => array('destination' => 'user/' . $account->uid)
      );
    }
  }
  if (!empty($links)) {
    $form['main_avatar']['add_links'] = array(
        '#theme' => 'links',
        '#links' => $links,
        '#attributes' => array(
          'class' => array('links', 'inline', 'operations')
      )
    );
  }
  // Make sure our validation and submits get called.
  $form['#validate'][] = 'alter_ego_user_profile_form_validate';
  $form['#submit'][] = 'alter_ego_user_profile_form_submit';
}
/**
 * Verify that the selected avatar exists and that the user owns it.
 * This could only happen if the user hacks the form.
 *
 * @param array $form
 * @param array $form_state
 */
function alter_ego_user_profile_form_validate(&$form, &$form_state) {
  $values = $form_state['values'];
  $user = $form_state['user'];
  if (!empty($values['mainaid'])) {
    $avatar = avatar_load($values['mainaid']);
    if (!empty($avatar->aid)) {
      if ($avatar->uid != $avatar->uid) {
        form_set_error('mainaid', 'You do not own this avatar.');
      }
    }
    else {
      form_set_error('mainaid', 'Invalid Character');
    }
  }
}

/**
 * Save user's primary avatar.
 *
 * @param array $form
 * @param array $form_state
 */
function alter_ego_user_profile_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if (!empty($values['mainaid'])) {
    if (!empty($values['uid'])) {
      // See if this user has an old primary we need to unset as main.
      $oldmainaid = alter_ego_get_main_aid($values['uid']);
      if ($oldmainaid && ($oldmainaid != $values['mainaid'])) {
        $oldtoon = avatar_load($oldmainaid);
        $oldtoon->is_main = 0;
        avatar_save($oldtoon);
      }
      
      $avatar = avatar_load($values['mainaid']);
      if (!$avatar->is_main) {
        $avatar->is_main = 1;
        avatar_save($avatar);
        alter_ego_set_current_aid($avatar->aid, $values['uid']);
      }
    }
  }
}

/**
 * Implementation of hook_user_load()
 *
 * Load main aids when user account is loaded.
 *
 * @param array $users
 */
function alter_ego_user_load($users) {
  $usertoons = array();
  foreach ($users as $user) {
    $aid = alter_ego_get_main_aid($user->uid);
    if (!empty($aid)) {
      $useraids[$user->uid] =  $aid;
    }
  }
  if (!empty($useraids)) {
    $avatars = avatar_load_multiple($useraids);
    foreach ($useraids as $uid => $useraid) {
      if (array_key_exists($uid, $avatars)) {
        $users[$uid]->avatar = $avatars[$uid];
      }
    }
  }
}

/**
 * Replace the user's name with their avatar's name.
 * TODO: Use preloaded avatar.
 *
 * @param string $name
 * @param object $account
 */
function alter_ego_username_alter(&$name, $account) {
  $aid = alter_ego_get_main_aid($account->uid);
  if ($aid) {
    $avatar = avatar_load($aid);
    if ($avatar) {
      $name = $avatar->name;
    }
  }
}

/**
 * Replace user's picture with their toons picture.
 *
 * @param array $variables
 */
function alter_ego_preprocess_user_picture(&$variables) {
  //if (variable_get('user_pictures', 0)) {
    $account = $variables['account'];
    if ($account->uid) {
      $aid = alter_ego_get_main_aid($account->uid);
      if ($aid) {
        $avatar = avatar_load($aid);
        if ($avatar) {
          
          $filepath = module_invoke_all('avatar_picture_url', $avatar);
          
          if ($filepath) {
            // module_invoke_all returns an array.
            $filepath = reset($filepath);
            $alt = t("@user's picture", array('@user' => format_username($account)));
            
            if (module_exists('image') && file_valid_uri($filepath) && $style = variable_get('user_picture_style', '')) {
              $variables['user_picture'] = theme('image_style', array('style_name' => $style, 'path' => $filepath, 'alt' => $alt, 'title' => $alt));
            }
            else {
              $variables['user_picture'] = theme('image', array('path' => $filepath, 'alt' => $alt, 'title' => $alt));
            }
            if (!empty($account->uid)) {
              if (user_access('view any ' . $avatar->type . ' avatar')) {
                $attributes = array('attributes' => array('title' => t('View avatar profile.')), 'html' => TRUE);
                $variables['user_picture'] = l($variables['user_picture'], "avatar/" . $avatar->aid, $attributes);
              }
            }
          }
        }
      }
    }
  //}
}

/** AUTHOR_PANE.MODULE HOOKS **/

/**
 * Author Pane template.
 */
function alter_ego_preprocess_author_pane(&$variables) {
  $temp = array(
    'account' => $variables['account']
  );
  alter_ego_preprocess_user_picture($temp);
  if (!empty($temp['user_picture'])) {
    $variables['picture'] = $temp['user_picture'];
  }
}


/**
 * Implement hook_user_delete().
 *
 * Clean up data when a user is deleted.
 *
 * @param user $account
 */
function alter_ego_user_delete($account) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'avatar', '=')
        ->propertyCondition('uid', $account->uid, '=');
  $result = $query->execute();
  if (!empty($result['avatar'])) {
    foreach ($result['avatar'] as $record) {
      $aids[] = $record->aid;
    }
    avatar_delete_multiple($aids);
  }
}


/**
 * Helper function to get the attached avatar->aids that this user owns.
 *
 * @param integer $uid
 */
function alter_ego_get_user_aids($uid = NULL) {
  global $user;
  if (empty($uid)) {
    $uid = $user->uid;
  }
  $cached_aids = &drupal_static(__FUNCTION__);
  if (!isset($cached_aids)) {
    $cached_aids = array();
  }
  if (!isset($cached_aids[$uid])) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'avatar', '=');
    $query->propertyCondition('uid', $uid, '=');
    $query->propertyOrderBy('weight', 'ASC');
    $result = $query->execute();
    $cached_aids[$uid] = array();
    if (!empty($result['avatar'])) {
      foreach ($result['avatar'] as $record) {
        $cached_aids[$uid][] = $record->aid;
      }
    }
  }
  return $cached_aids[$uid];
}

/**
 * Helper function to get the attached avatars that this user owns.
 *
 * @param $uid
 *   User ID or current user.
 *
 * @return
 *   Array of toons owned by user.
 */
function alter_ego_get_user_avatars($uid = NULL) {
  global $user;
  if (empty($uid)) {
    $uid = $user->uid;
  }
  $cached_avatars = &drupal_static(__FUNCTION__);
  if (!isset($cached_avatars)) {
    $cached_avatars = array();
  }
  if (!isset($cached_avatars[$uid])) {
    $cached_avatars[$uid] = array();
    $aids = alter_ego_get_user_aids($uid);
    if (!empty($aids)) {
      $cached_avatars[$uid] = entity_load('avatar', $aids);
    }
  }
  return $cached_avatars[$uid];
}

/**
 * Helper function to set the user's currently selected avatar->aid.
 *
 * @param integer $aid
 * @param integer $uid
 */
function alter_ego_set_current_aid($aid, $uid = NULL) {
  global $user;
  if (empty($uid)) {
    $uid = $user->uid;
  }
  if (!empty($aid) && !empty($uid)) {
    $aids = alter_ego_get_user_aids($uid);
    if (in_array($aid, $aids)) {
      // Place tid at the front of the list.
      $new_order = array($aid);
      $weight = 0;
      db_update('avatar')->fields(array('weight' => $weight++))->condition('aid', $aid)->execute();
      foreach ($aids as $id) {
        if ($id != $aid) {
          db_update('avatar')->fields(array('weight' => $weight++))->condition('aid', $id)->execute();
        }
      }
      drupal_static_reset('alter_ego_get_user_aids');
      drupal_static_reset('alter_ego_get_user_avatars');
    }
  }
}

/**
* Helper function to get the user's currently selected avatar->aid.
*
* @param integer $uid
*/
function alter_ego_get_current_aid($uid = NULL) {
  return reset(alter_ego_get_user_aids($uid));
}


/**
 * Helper function to get this user's main avatar.
 *
 * @param integer $uid
 */
function alter_ego_get_main_aid($uid = NULL) {
  global $user;
  if (empty($uid)) {
    $uid = $user->uid;
  }
  $cached_aid = &drupal_static(__FUNCTION__);
  if (!isset($cached_aid)) {
    $cached_aid = array();
  }
  if (!isset($cached_aid[$uid])) {
    $cached_aid[$uid] = FALSE;
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'avatar', '=');
    $query->propertyCondition('uid', $uid, '=');
    $query->propertyCondition('is_main', 1, '=');
    $result = $query->execute();
    if (!empty($result['avatar'])) {
      foreach ($result['avatar'] as $record) {
        $cached_aid[$uid] = $record->aid;
      }
    }
  }
  return $cached_aid[$uid];
}
